module bancho.ratelimit;
@safe:

import core.time;

import vibe.core.core;
import vibe.core.sync;

/// Params:
/// 	messages = Maximum allowed messages in the given timespan
/// 	timespan = Timespan to check messages in
struct ChatRatelimiter(int maxMessages, Duration timespan)
{
	/// Delay to always apply to avoid large gaps.
	Duration baseDelay = timespan / maxMessages / 2; // default to half an average cycle so if messages are going to be spammed, half of timespan is the maximum idle time
	/// Log of all sent message times.
	MonoTime[maxMessages] times;
	/// Index where to insert in times array right now.
	int cycle;
	InterruptibleTaskMutex mutex;

	void load()
	{
		mutex = new InterruptibleTaskMutex();
	}

	/// Throws: InterruptException if task got interrupted
	void putWait(bool put)
	{
		if (!putWaitNothrow(put))
			throw new InterruptException();
	}

	/// Returns: false if it was interrupted
	bool putWaitNothrow(bool put) nothrow
	{
		try
		{
			mutex.lock();
		}
		catch (InterruptException)
		{
			return false;
		}
		catch (Exception e)
		{
			import core.exception : AssertError;

			throw new AssertError("InterruptibleTaskMutex.lock is not expected to throw anything else than InterruptException", __FILE__, __LINE__, e);
		}

		scope (exit)
			mutex.unlock();

		auto sinceFirst = MonoTime.currTime - times[cycle];
		auto sinceLast = MonoTime.currTime - times[(cycle + $ - 1) % $];
		if (sinceLast >= baseDelay && (sinceFirst >= timespan || times[cycle] == MonoTime.zero))
		{
			// do nothing
		}
		else if (sinceFirst >= timespan || times[cycle] == MonoTime.zero)
		{
			// sleep remaining of baseDelay
			if (waitForInterrupt(baseDelay - sinceLast))
				return false;
		}
		else
		{
			if (waitForInterrupt(baseDelay + (timespan - sinceFirst)))
				return false;
		}

		if (put)
		{
			times[cycle] = MonoTime.currTime;
			cycle = (cycle + 1) % times.length;
		}

		return true;
	}

	/// Returns true if a message can be sent instantly.
	bool peekInstant() nothrow
	{
		if (!mutex.tryLock())
			return false;
		mutex.unlock();
		auto sinceFirst = MonoTime.currTime - times[cycle];
		auto sinceLast = MonoTime.currTime - times[(cycle + $ - 1) % $];
		return sinceLast >= baseDelay && (sinceFirst >= timespan || times[cycle] == MonoTime.zero);
	}

	/// Returns true if a message can be after waiting at most the baseDelay.
	bool peekBase() nothrow
	{
		if (!mutex.tryLock())
			return false;
		mutex.unlock();
		auto sinceFirst = MonoTime.currTime - times[cycle];
		return sinceFirst >= timespan || times[cycle] == MonoTime.zero;
	}
}

unittest
{
	import std.conv;
	import vibe.core.log;

	auto start = MonoTime.currTime;
	ChatRatelimiter!(5, 1.seconds) ratelimit; // 6 messages in 3 seconds
	ratelimit.load();

	assert(ratelimit.baseDelay == 100.msecs);

	foreach (i; 0 .. 4)
	{
		logInfo("waiting at %s", MonoTime.currTime - start);
		assert(ratelimit.peekBase, i.to!string);
		ratelimit.putWait(true);
		assert(ratelimit.peekBase, i.to!string);
		assert(!ratelimit.peekInstant, i.to!string);
		sleep(ratelimit.baseDelay);
		assert(ratelimit.peekInstant, i.to!string);
	}
	ratelimit.putWait(true);
	assert(!ratelimit.peekInstant);
	sleep(ratelimit.baseDelay);
	assert(!ratelimit.peekInstant);
	assert(!ratelimit.peekBase);

	logInfo("done at %s", MonoTime.currTime - start);
	assert(MonoTime.currTime - start > 450.msecs && MonoTime.currTime - start < 550.msecs,
			"took " ~ (MonoTime.currTime - start).toString ~ " instead of expected 0.5s");
	logInfo("waiting at %s", MonoTime.currTime - start);
	ratelimit.putWait(true);
	assert(MonoTime.currTime - start > 1050.msecs && MonoTime.currTime - start < 1150.msecs,
			"took " ~ (MonoTime.currTime - start).toString ~ " instead of expected 1s");
}

// public rooms: 60 messages / minute
// private channels: 300 messages / minute
struct BanchoRatelimiter(int publicMax = 6, Duration publicTime = 6.seconds,
		int privateMax = 5, Duration privateTime = 1.seconds)
{
	ChatRatelimiter!(publicMax, publicTime) publicLimit;
	ChatRatelimiter!(privateMax, privateTime) privateLimit;

	void load()
	{
		publicLimit.load();
		privateLimit.load();
	}

	void putWait(string channel, bool put)
	{
		isPublic(channel) ? publicLimit.putWait(put) : privateLimit.putWait(put);
	}

	bool putWaitNothrow(string channel, bool put) nothrow
	{
		return isPublic(channel) ? publicLimit.putWaitNothrow(put) : privateLimit.putWaitNothrow(put);
	}

	bool peekInstant(string channel) nothrow
	{
		return isPublic(channel) ? publicLimit.peekInstant() : privateLimit.peekInstant();
	}

	bool peekBase(string channel) nothrow
	{
		return isPublic(channel) ? publicLimit.peekBase() : privateLimit.peekBase();
	}

}

private bool isPublic(string channel) nothrow
{
	if (channel.length && channel[0] == '#')
	{
		// channel, so public
		return true;
	}
	else
	{
		// doesn't start with #, must be a player, so private
		return false;
	}
}

package bool waitForInterrupt(Duration d) nothrow
{
	try
	{
		sleep(d);
		return false;
	}
	catch (InterruptException)
	{
		return true;
	}
	catch (Exception e)
	{
		import core.exception : AssertError;

		throw new AssertError("sleep is not expected to throw anything else than InterruptException", __FILE__, __LINE__, e);
	}
}
