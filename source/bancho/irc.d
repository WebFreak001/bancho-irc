/// Provides an IRC connection to Bancho (osu!'s server system) and access to its commands (Multiplayer room creation)
module bancho.irc;
@safe:

import core.atomic;
import core.time;

import std.algorithm;
import std.array;
import std.conv;
import std.datetime.stopwatch;
import std.datetime.systime;
import std.datetime.timezone;
import std.exception : assumeUnique;
import std.functional;
import std.path;
import std.string;
import std.traits : isCallable, Parameters, ReturnType;
import std.typecons;
import std.uni : asLowerCase;

import vibe.core.core;
import vibe.core.log;
import vibe.core.net;
import vibe.stream.operations : readLine;

import tinyevent;

import bancho.ratelimit;

private
alias NothrowEvent(Args...) = void delegate(Args) @safe nothrow[];

/// Username of BanchoBot (for sending !mp commands & checking source)
static immutable string banchoBotNick = "BanchoBot";

enum SettingsLineMinLength = 55;

/// Replaces spaces with underscores in an osu! username
auto fixUsername(scope return inout(char)[] username)
{
	return username.replace(" ", "_");
}

unittest
{
	char[] a;
	const(char)[] ca;
	immutable(char)[] ia;

	static assert(is(typeof(fixUsername(a)) == char[]));
	static assert(is(typeof(fixUsername(ca)) == const(char)[]));
	static assert(is(typeof(fixUsername(ia)) == immutable(char)[]));
}

private
{
	auto extractUsername(scope const(char)[] part) nothrow
	{
		auto i = part.representation.countUntil('!');
		if (i == -1 || !part.length)
			return part[0 .. $];
		else
			return part[1 .. i];
	}

	void sendLine(TCPConnection conn, scope const(char)[] line)
	{
		logDebugV("send %s", line);
		conn.write(line);
		conn.write("\r\n");
		conn.flush();
	}

	void banchoAuthenticate(TCPConnection conn, scope const(char)[] host, scope const(char)[] username, scope const(char)[] password)
	{
		auto fixed = username.fixUsername;
		conn.sendLine("CAP LS 302");
		logDebugV("send PASS ******");
		conn.write("PASS " ~ password ~ "\r\n");
		conn.sendLine("NICK " ~ fixed);
		conn.sendLine("USER " ~ fixed ~ " " ~ fixed ~ " " ~ host ~ " :" ~ fixed);
	}

	void privMsg(TCPConnection conn, scope const(char)[] destination, scope const(char)[] message)
	{
		conn.sendLine("PRIVMSG " ~ destination ~ " :" ~ message);
	}

	void banchoQuit(TCPConnection conn)
	{
		conn.sendLine("QUIT :Leaving");
	}

	void tryBanchoQuit(TCPConnection conn) nothrow
	{
		try
		{
			conn.sendLine("QUIT :Leaving");
		}
		catch (Exception e)
		{
			logException(e, "failed quitting IRC");
		}
	}
}

/// Represents a simple sent message from bancho
struct Message
{
	/// User who wrote this message
	string sender;
	/// Target channel (#channel) or username
	string target;
	/// content of the message
	string message;

	/// Serializes to "[sender] -> [target]: [message]"
	string toString()
	{
		return sender ~ " -> " ~ target ~ ": " ~ message;
	}
}

/// Represents a topic change event (in the case of a multi room being created)
struct TopicChange
{
	///
	string channel, topic;
}

/// Represents a user quitting or joining the IRC ingame or via web/irc
struct Quit
{
	///
	string user;
	/// Mostly one out of:
	/// - "ping timeout 80s"
	/// - "quit"
	/// - "replaced (None 0ea7ac91-60bf-448b-adda-bc6b8d096dc7)"
	/// - "replaced (Supporter c83820bb-a602-43f0-8f17-04718e34b72d)"
	string reason;
}

/// Represents a join/leave event.
struct MembershipEvent
{
	///
	enum Type
	{
		///
		join,
		///
		part,
		///
		notFound
	}

	///
	string channel;
	///
	Type type;
}

///
struct BeatmapInfo
{
	/// b/ ID of the beatmap, extracted from url. Empty if couldn't be parsed
	string id;
	/// URL to the beatmap
	string url;
	/// Artist + Name + Difficulty
	string name;

	/// Parses a map in the format `Ariabl'eyeS - Kegare Naki Bara Juuji (Short ver.) [Rose†kreuz] (https://osu.ppy.sh/b/1239875)`
	static BeatmapInfo parseChange(string map)
	{
		BeatmapInfo ret;
		if (map.endsWith(")"))
		{
			auto start = map.lastIndexOf("(");
			ret.name = map[0 .. start].strip;
			ret.url = map[start + 1 .. $ - 1];
			if (ret.url.startsWith("https://osu.ppy.sh/b/"))
				ret.id = ret.url["https://osu.ppy.sh/b/".length .. $];
		}
		else
			ret.name = map;
		return ret;
	}
}

/// Utility mixin template implementing dynamic timeout based event subscribing + static backlog
/// Creates methods waitFor<fn>, process[fn], fetchOld[fn]Log, clear[fn]Log
mixin template Processor(string fn, Arg, size_t backlog, Duration ttl)
{
	struct Backlog
	{
		long at;
		Arg value;
	}

	bool delegate(Arg) nothrow @safe[] processors;
	// keep a few around for events called later
	Backlog[backlog] backlogs;

	void process(Arg arg) nothrow
	{
		static if (is(typeof(mixin("this.preProcess" ~ fn))))
			mixin("this.preProcess" ~ fn ~ "(arg);");
		foreach_reverse (i, proc; processors)
		{
			if (proc(arg))
			{
				processors[i] = processors[$ - 1];
				processors.length--;
				return;
			}
		}
		auto now = Clock.currStdTime;
		auto i = backlogs[].minIndex!"a.at < b.at";
		if (backlogs[i].at != 0 && now - backlogs[i].at < ttl.total!"hnsecs")
		{
			static if (is(Arg == Quit))
				logTrace("Disposing " ~ fn ~ " %s", backlogs[i].value);
			else
				logDebugV("Disposing " ~ fn ~ " %s", backlogs[i].value);
		}
		backlogs[i].at = now;
		backlogs[i].value = arg;
	}

	bool tryWaitFor(bool delegate(Arg) nothrow @safe check, Duration timeout, out Arg ret) nothrow
	{
		auto now = Clock.currStdTime;
		foreach (ref log; backlogs[].sort!"a.at < b.at")
		{
			if (log.at == 0 || now - log.at > ttl.total!"hnsecs")
				continue;
			if (check(log.value))
			{
				log.at = 0;
				ret = log.value;
				return true;
			}
		}
		if (timeout <= Duration.zero)
			return false;
		bool got = false;
		auto del = delegate(Arg msg) nothrow @safe {
			if (check(msg))
			{
				ret = msg;
				got = true;
				return true;
			}
			return false;
		};
		scope (failure)
			processors = processors.remove!(a => a == del, SwapStrategy.unstable);
		processors ~= del;
		StopWatch sw;
		sw.start();
		while (!got && sw.peek < timeout)
			if (waitForInterrupt(10.msecs))
				break;
		sw.stop();
		return got;
	}

	Arg waitFor(bool delegate(Arg) nothrow @safe check, Duration timeout)
	{
		Arg res;
		if (!tryWaitFor(check, timeout, res))
			throw new WaitTimeoutException();
		return res;
	}

	Arg[] fetchOldLog(bool delegate(Arg) nothrow @safe check, bool returnIt = true) nothrow
	{
		Arg[] ret;
		foreach (ref log; backlogs[].sort!"a.at < b.at")
		{
			if (log.at == 0)
				continue;
			if (check(log.value))
			{
				log.at = 0;
				if (returnIt)
					ret ~= log.value;
			}
		}
		return ret;
	}

	void clearLog() @safe nothrow
	{
		backlogs[] = Backlog.init;
		processors.length = 0;
	}

	mixin("alias processors" ~ fn ~ " = processors;");
	mixin("alias backlogs" ~ fn ~ " = backlogs;");
	mixin("alias tryWaitFor" ~ fn ~ " = tryWaitFor;");
	mixin("alias waitFor" ~ fn ~ " = waitFor;");
	mixin("alias process" ~ fn ~ " = process;");
	mixin("alias fetchOld" ~ fn ~ "Log = fetchOldLog;");
	mixin("alias clear" ~ fn ~ "Log = clearLog;");
}

/// Represents a Bancho IRC connection.
/// Examples:
/// ---
/// BanchoBot bot = new BanchoBot("User", "hunter2");
/// runTask({
///   while (true)
///   {
///     bot.connect();
///     logDiagnostic("Got disconnected from bancho...");
///     sleep(2.seconds);
///   }
/// });
/// ---
class BanchoBot
{
	version (D_Ddoc)
	{
		/// list of event subscribers. Gets removed automatically when called and returns true, otherwise caller has to remove it.
		bool delegate(Message)[] processorsMessage;
		bool delegate(Quit)[] processorsQuit; /// ditto
		bool delegate(TopicChange)[] processorsTopic; /// ditto
		bool delegate(MembershipEvent)[] processorsMembership; /// ditto

		/// list of backlog which hasn't been handled by any event subscribers, oldest one will always be replaced on new ones.
		Message[256] backlogsMessage;
		Quit[256] backlogsQuit; /// ditto
		TopicChange[8] backlogsTopic; /// ditto
		MembershipEvent[8] backlogsMembership; /// ditto

		/// Waits for an event or returns one which is in the backlog already. Removes matching backlog entries.
		/// Throws: WaitTimeoutException on timeout (for non try- prefix)
		/// Returns: boolean if something was found within timeout (for try- prefix) or the result (for non try- prefix)
		/// Params:
		///   check = a delegate checking for which object to check for. Return true to return this object & not add it to the backlog.
		///   timeout = the timeout after which to interrupt the waiting task
		bool tryWaitForMessage(bool delegate(Message) @safe nothrow check, Duration timeout, out Message result) nothrow;
		/// ditto
		bool tryWaitForQuit(bool delegate(Quit) @safe nothrow check, Duration timeout, out Quit result) nothrow;
		/// ditto
		bool tryWaitForTopic(bool delegate(TopicChange) @safe nothrow check, Duration timeout, out TopicChange result) nothrow;
		/// ditto
		bool tryWaitForMembership(bool delegate(MembershipEvent) @safe nothrow check, Duration timeout, out MembershipEvent result) nothrow;
		/// ditto
		Message waitForMessage(bool delegate(Message) @safe nothrow check, Duration timeout);
		/// ditto
		Quit waitForQuit(bool delegate(Quit) @safe nothrow check, Duration timeout);
		/// ditto
		TopicChange waitForTopic(bool delegate(TopicChange) @safe nothrow check, Duration timeout);
		/// ditto
		MembershipEvent waitForMembership(bool delegate(MembershipEvent) @safe nothrow check, Duration timeout);

		/// Calls all event subscribers to try to match this object, otherwise replace the oldest element in the backlog with this.
		/// Throws: anything thrown in event subscribers will get thrown here too.
		void processMessage(Message message) nothrow;
		/// ditto
		void processQuit(Quit quit) nothrow;
		/// ditto
		void processTopic(TopicChange change) nothrow;
		/// ditto
		void processMembership(MembershipEvent change) nothrow;

		/// Goes through the backlog and removes and optionally returns all matching objects.
		/// Params:
		///   check = a delegate checking for which object to check for. Return true to return this object & removing it from the backlog.
		///   returnIt = pass true to return the list of objects (GC), pass false to simply return an empty list and only remove from the backlog.
		Message[] fetchOldMessageLog(bool delegate(Message) @safe nothrow check, bool returnIt = true) nothrow;
		/// ditto
		Quit[] fetchOldQuitLog(bool delegate(Quit) @safe nothrow check, bool returnIt = true) nothrow;
		/// ditto
		TopicChange[] fetchOldTopicLog(bool delegate(TopicChange) @safe nothrow check, bool returnIt = true) nothrow;
		/// ditto
		MembershipEvent[] fetchOldMembershipLog(bool delegate(MembershipEvent) @safe nothrow check, bool returnIt = true) nothrow;

		/// Clears all backlog & removes all event listeners for the object type.
		void clearMessageLog() nothrow;
		/// ditto
		void clearQuitLog() nothrow;
		/// ditto
		void clearTopicLog() nothrow;
		/// ditto
		void clearMembershipLog() nothrow;
	}
	else
	{
		mixin Processor!("Message", Message, 256, 5.seconds);
		mixin Processor!("Quit", Quit, 256, 20.seconds);
		mixin Processor!("Topic", TopicChange, 8, 30.seconds);
		mixin Processor!("Membership", MembershipEvent, 8, 20.seconds);
	}

	///
	OsuRoom[] rooms;
	///
	TCPConnection client;
	/// Credentials to use for authentication when connecting.
	string username, password;
	/// IRC host to connect to.
	string host;
	/// IRC port to use to connect.
	ushort port;
	/// Event emitted on a private message to the bot.
	NothrowEvent!Message onDirectMessage;
	/// true after the first line has been received.
	bool gotLine;
	/// Event emitted on a successful login.
	NothrowEvent!() onAuthenticated;
	/// Event emitted for every motd line.
	NothrowEvent!string onMotdLine;

	/// Set to true to slow down message sending to the limit allowed on osu.
	bool doRatelimit;
	/// Bancho ratelimiter instance.
	BanchoRatelimiter!() ratelimiter;

	/// Prepares a bancho IRC connection with username & password (can be obtained from https://osu.ppy.sh/p/irc)
	this(string username, string password, string host = "irc.ppy.sh", ushort port = 6667)
	{
		if (!password.length)
			throw new Exception("Password can't be empty");
		this.username = username;
		this.password = password;
		this.host = host;
		this.port = port;
		ratelimiter.load();
	}

	/// Clears all logs, called by connect
	void clear() nothrow
	{
		clearMessageLog();
		clearTopicLog();
		clearQuitLog();
		clearMembershipLog();
		gotLine = false;
	}

	/// Connects to `this.host:this.port` (irc.ppy.sh:6667) and authenticates with username & password. Blocks and processes all messages sent by the TCP socket. Recommended to be called in runTask.
	/// Cleans up on exit properly and is safe to be called again once returned.
	/// Returns: false immediately if credentials are invalid or true after successful connection is closed.
	bool connect() nothrow
	{
		clear();

		try
		{
			client = connectTCP(host, port);
		}
		catch (Exception e)
		{
			logException(e, "connectTCP fail for host " ~ host);
			return false;
		}
		scope (exit)
			client.close();
		try
		{
			client.banchoAuthenticate(host, username, password);
		}
		catch (Exception e)
		{
			logException(e, "banchoAuthenticate fail");
			return false;
		}
		scope (exit)
			if (client.connected)
				client.tryBanchoQuit();

		try
		{
			while (client.connected)
			{
				if (!client.waitForData)
					break;
				// :Stepan1404!cho@ppy.sh QUIT :quit
				char[] line = cast(char[]) client.readLine(1024, "\n");
				if (line.endsWith('\r'))
					line = line[0 .. $ - 1];
				logTrace("recv %s", line);
				auto parts = line.splitter(' ');
				size_t eaten = 0;
				auto user = parts.front;
				if (user == "PING")
				{
					line[1] = 'O'; // P[I]NG -> P[O]NG;
					client.sendLine(line);
					continue;
				}
				eaten += parts.front.length + 1;
				parts.popFront;
				auto cmd = parts.front;
				eaten += parts.front.length + 1;
				parts.popFront;
				if (isNumeric(cmd))
				{
					string lineDup = line.idup;
					auto cmdi = cmd.to!int;
					if (cmdi == 464)
						return false;
					runTask(&processNumeric, cmdi, lineDup, lineDup[eaten .. $]);
				}
				else if (cmd == "PART")
				{
					// >> :WebFreak!cho@ppy.sh PART :#mp_44448104
					if (user.extractUsername == username)
					{
						string channel = line[eaten + 1 .. $].idup;
						processMembership(MembershipEvent(channel, MembershipEvent.Type.part));
						disownChannel(channel, true);
					}
				}
				else if (cmd == "JOIN")
				{
					// >> :WebFreak!cho@ppy.sh JOIN :#mp_44448993
					if (user.extractUsername == username)
					{
						string channel = line[eaten + 1 .. $].idup;
						processMembership(MembershipEvent(channel, MembershipEvent.Type.join));
					}
				}
				else if (cmd == "QUIT")
					runTask(&processQuit, Quit(user.extractUsername.idup, line[eaten + 1 .. $].idup));
				else if (cmd == "PRIVMSG")
				{
					auto target = parts.front;
					eaten += parts.front.length + 1;
					parts.popFront;
					if (line[eaten] != ':')
						throw new Exception("Malformed message received: " ~ line.idup);
					runTask(&processMessage, Message(user.extractUsername.idup,
							target.idup, line[eaten + 1 .. $].idup));
				}
				else
					logDiagnostic("Unknown line %s", line.idup);
				gotLine = true;
			}
		}
		catch (Exception e)
		{
			logException(e, "Exception in IRC task");
		}
		return true;
	}

	void waitUntilLoggedIn()
	{
		sleep(10.msecs);
		while (client.connected && !gotLine)
			sleep(10.msecs);
	}

	~this()
	{
		disconnect();
	}

	/// Disconnects & closes the TCP socket.
	void disconnect() nothrow
	{
		if (client.connected)
		{
			client.tryBanchoQuit();
			client.close();
		}
	}

	/// Processes messages meant for mutliplayer rooms to update their state.
	/// called by mixin template
	void preProcessMessage(Message message) nothrow
	{
		foreach (room; rooms)
			if (room.open && message.target == room.channel)
				runTask((OsuRoom room, Message message) @safe nothrow {
					room.onMessage.emit(message);
				}, room, message);
		if (!message.target.startsWith("#"))
			runTask((Message message) @safe nothrow {
				onDirectMessage.emit(message);
			}, message);
		if (message.sender != banchoBotNick)
			return;
		foreach (room; rooms)
		{
			if (room.open && message.target == room.channel)
			{
				try
				{
					if (message.message == "All players are ready")
					{
						runTask((OsuRoom room) nothrow { room.onPlayersReady.emit(); }, room);
						foreach (ref slot; room.slots)
							if (slot != OsuRoom.Settings.Player.init)
								slot.ready = true;
						break;
					}
					if (message.message == "Countdown finished")
					{
						runTask((OsuRoom room) nothrow {
							room.timerRunning = false;
							room.timerIsStart = false;
							room.onCountdownFinished.emit();
						}, room);
						break;
					}
					if (message.message == "Countdown aborted")
					{
						runTask((OsuRoom room) nothrow {
							room.timerRunning = false;
							room.onCountdownAborted.emit();
						}, room);
						break;
					}
					if (message.message == "Aborted the match")
					{
						runTask((OsuRoom room) nothrow {
							room.inProgress = false;
							room.onMatchAborted.emit();
						}, room);
						break;
					}
					if (message.message == "The match is not in progress")
					{
						runTask((OsuRoom room) nothrow { room.inProgress = false; }, room);
						break;
					}
					if (message.message == "Host is changing map...")
					{
						runTask((OsuRoom room) nothrow { room.onBeatmapPending.emit(); }, room);
						break;
					}
					if (message.message == "The match has started!")
					{
						runTask((OsuRoom room) nothrow {
							room.inProgress = true;
							room.timerRunning = false;
							room.timerIsStart = true;
							room.onMatchStart.emit();
						}, room);
						break;
					}
					if (message.message == "The match has finished!")
					{
						room.processMatchFinish();
						break;
					}
					if (message.message.startsWithString("Beatmap changed to: "))
					{
						// Beatmap changed to: Ariabl'eyeS - Kegare Naki Bara Juuji (Short ver.) [Rose†kreuz] (https://osu.ppy.sh/b/1239875)
						room.onBeatmapChanged.emit(BeatmapInfo.parseChange(
								message.message["Beatmap changed to: ".length .. $]));
						break;
					}
					if (message.message.startsWithString("Changed beatmap to https://osu.ppy.sh/b/"))
					{
						// Changed beatmap to https://osu.ppy.sh/b/1860433 Brian The Sun - Lonely Go! (TV Size)
						auto data = message.message["Changed beatmap to ".length .. $];
						auto space = data.indexOf(' ');
						if (space == -1)
							space = data.length;
						auto url = data[0 .. space];
						auto mapID = data["https://osu.ppy.sh/b/".length .. space];
						auto title = space < data.length ? data[space + 1 .. $] : null;
						room.onBeatmapChanged.emit(BeatmapInfo(mapID, url, title));
						break;
					}
					if (message.message.startsWithString("Changed match to size "))
					{
						room.processSize(message.message["Changed match to size ".length .. $].strip.to!ubyte);
						break;
					}
					if (message.message.endsWith(" left the game."))
					{
						room.processLeave(message.message[0 .. $ - " left the game.".length]);
						break;
					}
					if (message.message.endsWith(" changed to Blue"))
					{
						room.processTeam(message.message[0 .. $ - " changed to Blue.".length], Team.Blue);
						break;
					}
					if (message.message.endsWith(" changed to Red"))
					{
						room.processTeam(message.message[0 .. $ - " changed to Red.".length], Team.Red);
						break;
					}
					if (message.message.endsWith(" became the host."))
					{
						room.processHost(message.message[0 .. $ - " became the host.".length]);
						break;
					}
					if (message.message == "Cleared match host")
					{
						room.processHostClear();
						break;
					}
					size_t index;
					if ((index = message.message.indexOf(" joined in slot ")) != -1)
					{
						if (message.message.endsWith("."))
							message.message.length--;
						string s = message.message[index + " joined in slot ".length .. $];
						Team team = Team.None;
						auto forTeam = s.indexOf(" for team ");
						if (forTeam != -1)
						{
							auto teamStr = s[forTeam + " for team ".length .. $];
							if (teamStr.asLowerCase.startsWith("blue"))
								team = Team.Blue;
							else if (teamStr.asLowerCase.startsWith("red"))
								team = Team.Red;
						}
						// UnlimitedGamez joined in slot 1 for team blue.
						// UnlimitedGamez joined in slot 2 for team red.
						room.processJoin(message.message[0 .. index], cast(ubyte)(s.parse!ubyte - 1), team);
						break;
					}
					if ((index = message.message.indexOf(" moved to slot ")) != -1)
					{
						if (message.message.endsWith("."))
							message.message.length--;
						room.processMove(message.message[0 .. index],
								cast(ubyte)(message.message[index + " moved to slot ".length .. $].to!int - 1));
						break;
					}
					if ((index = message.message.indexOf(" finished playing (Score: ")) != -1)
					{
						string user = message.message[0 .. index];
						long score = message.message[index
							+ " finished playing (Score: ".length .. $ - ", PASSED).".length].to!long;
						bool pass = message.message.endsWith("PASSED).");
						room.processFinishPlaying(user, score, pass);
						break;
					}
					break;
				}
				catch (Exception e)
				{
					if (!room.fatal)
					{
						room.trySendMessage(
								"An internal exception occurred: " ~ e.msg ~ " in "
								~ e.file.baseName ~ ":" ~ e.line.to!string);
						room.fatal = true;
						logException(e, "fatal room exception");
					}
					break;
				}
			}
		}
	}

	void processNumeric(int num, string line, string relevantPart) nothrow
	{
		// internal function processing numeric commands
		if (num == 332)
		{
			// :cho.ppy.sh 332 WebFreak #mp_40121420 :multiplayer game #24545
			auto parts = relevantPart.representation.splitter(' ');
			size_t eaten;
			if (parts.empty || parts.front != username)
			{
				logInfo("Received topic change not made for us?! %s", relevantPart);
				return;
			}
			eaten += parts.front.length + 1;
			parts.popFront;
			if (parts.empty)
			{
				logInfo("Received broken topic change?! %s", relevantPart);
				return;
			}
			string channel = cast(string) parts.front;
			eaten += parts.front.length + 1;
			parts.popFront;
			if (parts.empty || !parts.front.length || parts.front[0] != ':')
			{
				logInfo("Malformed topic change: %s", relevantPart);
				return;
			}
			processTopic(TopicChange(channel, relevantPart[eaten + 1 .. $]));
		}
		else if (num == 403)
		{
			// << JOIN #mp_6515665
			// >> :cho.ppy.sh 403 WebFreak #mp_6515665 :No such channel #mp_6515665
			auto parts = relevantPart.representation.splitter(' ');
			if (parts.empty || parts.front != username)
			{
				logInfo("Received 'no such channel' not made for us?! %s", relevantPart);
				return;
			}
			parts.popFront;
			if (parts.empty)
			{
				logInfo("Received broken 'no such channel'?! %s", relevantPart);
				return;
			}
			string channel = cast(string) parts.front;
			disownChannel(channel, true);
			processMembership(MembershipEvent(channel, MembershipEvent.Type.notFound));
		}
		else if (num == 1)
		{
			onAuthenticated.emit();
		}
		else if (num == 375 || num == 372 || num == 376)
		{
			// :cho.ppy.sh 372 WebFreak :- boat:   https://twitter.com/banchoboat
			if (!relevantPart.startsWithString(username))
				return;

			relevantPart = relevantPart[username.length .. $];
			if (relevantPart.startsWithString(" :"))
				onMotdLine.emit(relevantPart[2 .. $]);
		}
		else
			logDebug("Got Numeric: %s %s", num, line);
	}

	/// Sends a message to a username or channel (#channel).
	void sendMessage(string channel, scope const(char)[] message)
	{
		sendMessage(channel, message, doRatelimit);
	}

	/// ditto
	void sendMessage(string channel, scope const(char)[] message, bool doRatelimit)
	{
		if (doRatelimit)
			ratelimit(channel, true);
		client.privMsg(channel.replace(' ', '_'), message.replace("\n", " "));
	}

	/// ditto
	bool trySendMessage(string channel, scope const(char)[] message) nothrow
	{
		try
		{
			sendMessage(channel, message);
			return true;
		}
		catch (Exception e)
		{
			logException(e, "failed messaging trySendMessage msg");
			return false;
		}
	}

	/// ditto
	bool trySendMessage(string channel, scope const(char)[] message, bool doRatelimit) nothrow
	{
		try
		{
			sendMessage(channel, message, doRatelimit);
			return true;
		}
		catch (Exception e)
		{
			logException(e, "failed messaging trySendMessage msg");
			return false;
		}
	}

	/// Waits for multiple messages sent at once and returns them.
	/// Params:
	///   check = delegate to check if the message matches expectations (author, channel, etc)
	///   timeout = timeout to wait for first message
	///   totalTimeout = total time to spend starting waiting for messages
	///   inbetweenTimeout = timeout for a message after the first message. totalTimeout + inbetweenTimeout is the maximum amount of time this function runs.
	Message[] waitForMessageBunch(bool delegate(Message) @safe nothrow check, Duration timeout,
			Duration totalTimeout = 5.seconds, Duration inbetweenTimeout = 300.msecs)
	{
		Message[] ret;
		try
		{
			StopWatch sw;
			sw.start();
			scope (exit)
				sw.stop();
			ret ~= waitForMessage(check, timeout);
			while (sw.peek < totalTimeout)
				ret ~= waitForMessage(check, inbetweenTimeout);
		}
		catch (WaitTimeoutException)
		{
		}
		return ret;
	}

	/// Creates a new managed room with a title and returns it.
	/// Automatically gets room ID & game ID.
	/// Throws: Exception if there are too many rooms or if nothing got sent by bancho.
	/// Throws: NoServerResponseException if bancho didn't respond.
	/// Returns: the room if able to create.
	OsuRoom createRoom(string title)
	{
		title = title.strip; // bancho strips whitespace from name
		sendMessage(banchoBotNick, "!mp make " ~ title);
		auto channel = this.waitForMembership(a => a.type == MembershipEvent.Type.join
				&& a.channel.startsWith("#mp_"), 10.seconds).channel;
		auto msg = this.waitForMessage(a => a.sender == banchoBotNick && a.target == username
				&& ((a.message.startsWithString("Created the tournament match ")
					&& a.message.canFind(channel["#mp_".length .. $]))
					|| a.message.startsWithString("You cannot create any more")), 10.seconds).message;
		if (!msg.length)
			throw new Exception("Bancho didn't send anything valid");
		// You cannot create any more tournament matches. Please close any previous tournament matches you have open.
		if (msg.startsWith("You cannot create any more"))
			throw new Exception("Can't create any rooms anymore");
		// "Created the tournament match https://osu.ppy.sh/mp/40080950 bob"
		if (msg.startsWith("Created the tournament match "))
			msg = msg["Created the tournament match ".length .. $];
		if (msg.startsWith("https://osu.ppy.sh/mp/"))
		{
			auto space = msg.indexOf(' ');
			if (space == -1)
				msg = msg["https://osu.ppy.sh/mp/".length .. $];
			else
				msg = msg["https://osu.ppy.sh/mp/".length .. space];
		}
		if (msg != channel["#mp_".length .. $])
			throw new Exception("Created and resolved to an unexpected room '#mp_" ~ msg
					~ "' while expecting '" ~ channel
					~ "'. Please try not to create rooms in parallel to avoid this.");

		auto topic = this.waitForTopic(a => a.channel == channel
				&& a.topic.startsWith("multiplayer game #"), 500.msecs).topic;
		auto room = new OsuRoom(this, channel, topic["multiplayer game #".length .. $]);
		rooms ~= room;
		return room;
	}

	/// Joins a room in IRC and creates the room object from it.
	/// Throws: NoServerResponseException if bancho didn't respond or ChannelNotFoundException if channel doesn't exist.
	/// Params:
	///     room = IRC Room name (starting with `#mp_`) where to send the messages in.
	///     game = optional string containing the osu://mp/ URL.
	OsuRoom fromUnmanaged(string room, string game = null)
	in
	{
		assert(room.startsWith("#mp_"));
	}
	do
	{
		joinChannel(room);
		auto type = this.waitForMembership(a => a.type.among!(MembershipEvent.Type.join,
				MembershipEvent.Type.notFound) && a.channel == room, 10.seconds).type;
		if (type == MembershipEvent.Type.notFound)
			throw new ChannelNotFoundException(room);
		auto obj = new OsuRoom(this, room, game);
		rooms ~= obj;
		return obj;
	}

	/// Sends a join command to a channel so messages can be sent in it.
	void joinChannel(string channel)
	{
		client.sendLine("JOIN " ~ channel);
	}

	/// Removes a channel from the managed channels.
	void disownChannel(string channel, bool gotClosed = false) nothrow
	{
		foreach_reverse (i, room; rooms)
		{
			if (room.channel == channel)
			{
				rooms = rooms.remove(i);
				if (gotClosed)
					room.processClosed();
				break;
			}
		}
	}

	/// internal function to remove a room from the managed rooms list
	void unmanageRoom(OsuRoom room) nothrow
	{
		rooms = rooms.remove!(a => a is room);
	}

	/// Manually wait until you can send a message again in a given channel.
	/// Params:
	///    channel = The channel where a message would be sent (starting with #) or a username.
	///    put = if false, the time won't get written to the ratelimit and it will just be waited for it.
	void ratelimit(string channel, bool put)
	{
		ratelimiter.putWait(channel, put);
	}

	/// ditto
	bool ratelimitNothrow(string channel, bool put) nothrow
	{
		return ratelimiter.putWaitNothrow(channel, put);
	}
}

/*
>> :WebFreak!cho@ppy.sh JOIN :#mp_40121420
<< WHO #mp_40121420
>> :BanchoBot!cho@cho.ppy.sh MODE #mp_40121420 +v WebFreak
>> :cho.ppy.sh 332 WebFreak #mp_40121420 :multiplayer game #24545
>> :cho.ppy.sh 333 WebFreak #mp_40121420 BanchoBot!BanchoBot@cho.ppy.sh 1518796852
>> :cho.ppy.sh 353 WebFreak = #mp_40121420 :@BanchoBot +WebFreak 
>> :cho.ppy.sh 366 WebFreak #mp_40121420 :End of /NAMES list.
>> :BanchoBot!cho@ppy.sh PRIVMSG WebFreak :Created the tournament match https://osu.ppy.sh/mp/40121420 bob
>> :cho.ppy.sh 324 WebFreak #mp_40121420 +nt
>> :cho.ppy.sh 329 WebFreak #mp_40121420 1518796852
>> :cho.ppy.sh 315 WebFreak #mp_40121420 :End of /WHO list.
<< PRIVMSG #mp_40121420 :!mp close
>> :WebFreak!cho@ppy.sh PART :#mp_40121420
>> :BanchoBot!cho@ppy.sh PRIVMSG #mp_40121420 :Closed the match
*/

/*
<WebFreak> !mp invite WebFreak
<BanchoBot> Invited WebFreak to the room
<BanchoBot> WebFreak joined in slot 1.
<BanchoBot> WebFreak moved to slot 2

<WebFreak> !mp host WebFreak
<BanchoBot> WebFreak became the host.
<BanchoBot> Changed match host to WebFreak
<BanchoBot> Beatmap changed to: bibuko - Reizouko Mitara Pudding ga Nai [Mythol's Pudding] (https://osu.ppy.sh/b/256839)

<WebFreak> !mp mods FI
<BanchoBot> Enabled FadeIn, disabled FreeMod

<WebFreak> !mp start
<BanchoBot> The match has started!
<BanchoBot> Started the match
<BanchoBot> WebFreak finished playing (Score: 487680, FAILED).
<BanchoBot> The match has finished!

aborting (esc):
<BanchoBot> WebFreak finished playing (Score: 300, PASSED).

<BanchoBot> WebFreak finished playing (Score: 113216, PASSED).
<BanchoBot> The match has finished!

<BanchoBot> All players are ready

<WebFreak> !mp start
<BanchoBot> The match has started!
<BanchoBot> Started the match

<WebFreak> !mp abort
<BanchoBot> Aborted the match

<BanchoBot> WebFreak moved to slot 3
<BanchoBot> WebFreak changed to Red
<BanchoBot> WebFreak changed to Blue
<BanchoBot> WebFreak moved to slot 1

<BanchoBot> Host is changing map...
<BanchoBot> Beatmap changed to: Aitsuki Nakuru - Krewrap no uta [Easy] (https://osu.ppy.sh/b/1292635)

<WebFreak> !mp settings
<BanchoBot> Room name: bob, History: https://osu.ppy.sh/mp/40081206
<BanchoBot> Beatmap: https://osu.ppy.sh/b/1292635 Aitsuki Nakuru - Krewrap no uta [Easy]
<BanchoBot> Team mode: HeadToHead, Win condition: Score
<BanchoBot> Active mods: Freemod
<BanchoBot> Players: 1
<BanchoBot> Slot 1  Not Ready https://osu.ppy.sh/u/1756786 WebFreak        [Host / Hidden]

<WebFreak> !mp settings
<BanchoBot> Room name: bob, History: https://osu.ppy.sh/mp/40081206
<BanchoBot> Beatmap: https://osu.ppy.sh/b/1292635 Aitsuki Nakuru - Krewrap no uta [Easy]
<BanchoBot> Team mode: HeadToHead, Win condition: Score
<BanchoBot> Active mods: HalfTime, Freemod
<BanchoBot> Players: 1
<BanchoBot> Slot 1  Not Ready https://osu.ppy.sh/u/1756786 WebFreak        [Host / Hidden, HardRock, SuddenDeath]

<WebFreak> !mp size 1
<BanchoBot> WebFreak left the game.
<BanchoBot> Changed match to size 1
<BanchoBot> WebFreak joined in slot 1.
*/

///
enum TeamMode
{
	///
	HeadToHead,
	///
	TagCoop,
	///
	TeamVs,
	///
	TagTeamVs
}

///
enum ScoreMode
{
	///
	Score,
	///
	Accuracy,
	///
	Combo,
	///
	ScoreV2
}

///
enum Team
{
	/// default, used when mode is not TeamVs/TagTeamVs
	None,
	///
	Red,
	///
	Blue
}

/// Represents a gamemode with integer mapping in the osu! api.
enum GameMode
{
	/// osu! standard mode
	osu,
	/// taiko mode
	taiko,
	/// catch the beat mode
	ctb,
	/// mania mode
	mania
}

///
enum Mod : string
{
	///
	Easy = "Easy",
	///
	NoFail = "NoFail",
	///
	HalfTime = "HalfTime",
	///
	HardRock = "HardRock",
	///
	SuddenDeath = "SuddenDeath",
	///
	DoubleTime = "DoubleTime",
	///
	Nightcore = "Nightcore",
	///
	Hidden = "Hidden",
	///
	FadeIn = "FadeIn",
	///
	Flashlight = "Flashlight",
	///
	Relax = "Relax",
	///
	Autopilot = "Relax2",
	///
	SpunOut = "SpunOut",
	///
	Key1 = "Key1",
	///
	Key2 = "Key2",
	///
	Key3 = "Key3",
	///
	Key4 = "Key4",
	///
	Key5 = "Key5",
	///
	Key6 = "Key6",
	///
	Key7 = "Key7",
	///
	Key8 = "Key8",
	///
	Key9 = "Key9",
	///
	KeyCoop = "KeyCoop",
	///
	ManiaRandom = "Random",
	///
	FreeMod = "FreeMod",
	///
	Mirror = "Mirror",
}

///
enum ModNumber
{
	///
	None,
	///
	NoFail = 1 << 0,
	///
	Easy = 1 << 1,
	///
	TouchDevice = 1 << 2,
	///
	Hidden = 1 << 3,
	///
	HardRock = 1 << 4,
	///
	SuddenDeath = 1 << 5,
	///
	DoubleTime = 1 << 6,
	///
	Relax = 1 << 7,
	///
	HalfTime = 1 << 8,
	///
	Nightcore = 1 << 9,
	///
	Flashlight = 1 << 10,
	///
	Auto = 1 << 11,
	///
	SpunOut = 1 << 12,
	///
	Autopilot = 1 << 13,
	///
	Perfect = 1 << 14,
	///
	Key4 = 1 << 15,
	///
	Key5 = 1 << 16,
	///
	Key6 = 1 << 17,
	///
	Key7 = 1 << 18,
	///
	Key8 = 1 << 19,
	///
	FadeIn = 1 << 20,
	///
	Random = 1 << 21,
	///
	Cinema = 1 << 22,
	///
	TargetPractice = 1 << 23,
	///
	Key9 = 1 << 24,
	///
	KeyCoop = 1 << 25,
	///
	Key1 = 1 << 26,
	///
	Key3 = 1 << 27,
	///
	Key2 = 1 << 28,
	///
	ScoreV2 = 1 << 29,
	///
	Mirror = 1 << 30,
	deprecated LastMod = 1 << 30,
}

/// Generates the short form for a mod (eg Hidden -> HD), can be more than 2 characters
string shortForm(Mod mod) @safe pure nothrow @nogc
{
	//dfmt off
	switch (mod)
	{
	case Mod.Easy: return "EZ";
	case Mod.NoFail: return "NF";
	case Mod.HalfTime: return "HT";
	case Mod.HardRock: return "HR";
	case Mod.SuddenDeath: return "SD";
	case Mod.DoubleTime: return "DT";
	case Mod.Nightcore: return "NC";
	case Mod.Hidden: return "HD";
	case Mod.FadeIn: return "FI";
	case Mod.Flashlight: return "FL";
	case Mod.Relax: return "RX";
	case Mod.Autopilot: return "AP";
	case Mod.SpunOut: return "SO";
	case Mod.Key1: return "K1";
	case Mod.Key2: return "K2";
	case Mod.Key3: return "K3";
	case Mod.Key4: return "K4";
	case Mod.Key5: return "K5";
	case Mod.Key6: return "K6";
	case Mod.Key7: return "K7";
	case Mod.Key8: return "K8";
	case Mod.Key9: return "K9";
	case Mod.KeyCoop: return "COOP";
	case Mod.ManiaRandom: return "RN";
	case Mod.Mirror: return "MR";
	case Mod.FreeMod:
	default: return mod;
	}
	//dfmt on
}

/// Checks if a username is a valid osu username for registering now. Note that older usernames MAY be longer or contain illegal character combinations.
/// Some character combinations are denied by user registration but are not checked for in here (such as admin).
///
/// A valid username according to this function is between 3 and 15 characters long, doesn't start or end with spaces (strip them before passing),
/// consists only out of a-z A-Z 0-9 _ [ ] - (SPACE) and doesn't contain any of these character combinations: `  ` (double space), `[]`.
bool isValidOsuUsername(scope const(char)[] username, size_t maxLen = 15) @safe pure nothrow @nogc
{
	import std.ascii : isAlphaNum;

	return username.length >= 3 && username.length <= maxLen && !username.startsWith(' ')
		&& !username.endsWith(' ') && username.representation.all!(c => c.isAlphaNum
				|| c == '_' || c == '[' || c == ']' || c == '-' || c == ' ')
		&& !username.canFind("  ") && !username.canFind("[]");
}

/// Cyclic int how to modify a message to avoid message-flooding protection from bancho.
shared int despamState = 0;

/// Alters an !mp command sent to bancho such that it doesn't get ignored because of flood protection because of sending identical commands.
/// The current implementation simply inserts alternating 0, 1 and 2 spaces after !mp.
const(char)[] despam(scope return const(char)[] command) @safe nothrow
{
	static immutable prefix = "!mp";
	if (command.startsWith(prefix))
	{
		int currentState = despamState.atomicOp!"+="(1) & 0b11;
		if (currentState == 0)
			return command;
		else if (currentState < 4)
		{
			char[] ret = new char[command.length + currentState];
			ret[0 .. prefix.length] = prefix;
			ret[prefix.length .. prefix.length + currentState] = ' ';
			ret[prefix.length + currentState .. $] = command[prefix.length .. $];
			return (() @trusted => assumeUnique(ret))();
		}
		else
			assert(false);
	}
	else
		return command;
}

unittest
{
	despamState = 0;
	assert(despam("!mp foo") == "!mp  foo");
	assert(despam("!mp foo") == "!mp   foo");
	assert(despam("!mp foo") == "!mp    foo");
	assert(despam("!mp foo") == "!mp foo");
	assert(despam("!mp foo") == "!mp  foo");
	assert(despam("!mp foo") == "!mp   foo");
	assert(despam("!mp foo") == "!mp    foo");
	assert(despam("!mp foo") == "!mp foo");
}

/// Runs some command up to 3 times whether or not checkResponse returns true. Expects a response from BanchoBot.
/// Throws: NoServerResponseException if the command never returned any output.
auto commandWithResponse(bool multiResponse = false)(BanchoBot bot,
	bool delegate(Message) @safe nothrow checkResponse,
	string channel,
	scope const(char)[] message,
	bool ratelimit,
	int retries = 3)
{
	static if (multiResponse)
		Message[] ret;
	else
		Message ret;
	if (!tryCommandWithResponseImpl!(multiResponse, typeof(ret), false)(bot, checkResponse, ret, channel, message, ratelimit, retries))
		throw new NoServerResponseException();
	return ret;
}

auto commandWithIgnoredResponse(bool multiResponse = false)(BanchoBot bot,
	bool delegate(Message) @safe nothrow checkResponse,
	string channel,
	scope const(char)[] message,
	bool ratelimit,
	int retries = 3)
{
	static if (multiResponse)
		Message[] ret;
	else
		Message ret;
	if (!tryCommandWithResponseImpl!(multiResponse, typeof(ret), false)(bot, checkResponse, ret, channel, message, ratelimit, retries))
		return typeof(ret).init;
	return ret;
}

pragma(inline, true)
bool tryCommandWithResponse(BanchoBot bot,
	bool delegate(Message) @safe nothrow checkResponse,
	out Message result,
	string channel,
	scope const(char)[] message,
	bool ratelimit,
	int retries = 3) nothrow
{
	return tryCommandWithResponseImpl!(false, Message, true)(bot, checkResponse, result, channel, message, ratelimit, retries);
}

pragma(inline, true)
bool tryCommandWithResponse(BanchoBot bot,
	bool delegate(Message) @safe nothrow checkResponse,
	out Message[] result,
	string channel,
	scope const(char)[] message,
	bool ratelimit,
	int retries = 3) nothrow
{
	return tryCommandWithResponseImpl!(true, Message[], true)(bot, checkResponse, result, channel, message, ratelimit, retries);
}

private bool tryCommandWithResponseImpl(bool multiResponse, Result, bool catchAll)(BanchoBot bot,
	bool delegate(Message) @safe nothrow checkResponse,
	out Result result,
	string channel,
	scope const(char)[] message,
	bool ratelimit,
	int retries = 3)
{
	while (retries > 0)
	{
		static if (catchAll)
		{
			try
			{
				bot.sendMessage(channel, despam(message), ratelimit);
			}
			catch (Exception)
			{
				return false;
			}
		}
		else
		{
			bot.sendMessage(channel, despam(message), ratelimit);
		}

		try
		{
			static if (multiResponse)
			{
				auto response = bot.waitForMessageBunch(a => a.target == channel
						&& a.sender == banchoBotNick && checkResponse(a), 3.seconds, 6.seconds, 400.msecs);
				if (response.length)
				{
					result = response;
					return true;
				}
			}
			else
			{
				result = bot.waitForMessage(a => a.target == channel
						&& a.sender == banchoBotNick && checkResponse(a), 3.seconds);
				return true;
			}
		}
		catch (WaitTimeoutException)
		{
		}
		catch (Exception e)
		{
			import core.exception : AssertError;

			throw new AssertError("tryCommandWithResponse has thrown something unexpected", __FILE__, __LINE__, e);
		}
		retries--;
		logDiagnostic("Sending command '%s' in channel %s failed, retrying in a second...",
				message, channel);

		static if (catchAll)
		{
			if (waitForInterrupt(1200.msecs))
				break;
		}
		else
		{
			sleep(1200.msecs);
		}
	}
	return false;
}

/// ditto
auto commandWithResponse(bool multiResponse = false)(OsuRoom room,
		bool delegate(Message) @safe nothrow checkResponse, scope const(char)[] message)
{
	return room.bot.commandWithResponse!(multiResponse)(checkResponse, room.channel, message, true);
}

/// ditto
auto commandWithIgnoredResponse(bool multiResponse = false)(OsuRoom room,
		bool delegate(Message) @safe nothrow checkResponse, scope const(char)[] message)
{
	return room.bot.commandWithIgnoredResponse!(multiResponse)(checkResponse, room.channel, message, true);
}

/// ditto
bool tryCommandWithResponse(OsuRoom room, bool delegate(Message) @safe nothrow checkResponse, out Message ret, scope const(char)[] message) nothrow
{
	return room.bot.tryCommandWithResponse(checkResponse, ret, room.channel, message, true);
}

/// ditto
bool tryCommandWithResponse(OsuRoom room, bool delegate(Message) @safe nothrow checkResponse, out Message[] ret, scope const(char)[] message) nothrow
{
	return room.bot.tryCommandWithResponse(checkResponse, ret, room.channel, message, true);
}

class NoServerResponseException : Exception
{
	this(string msg, string file = __FILE__, size_t line = __LINE__, Throwable nextInChain = null) pure nothrow @nogc @safe
	{
		super(msg, file, line, nextInChain);
	}

	this(string file = __FILE__, size_t line = __LINE__, Throwable nextInChain = null) pure nothrow @nogc @safe
	{
		super("did not get response from server in time", file, line, nextInChain);
	}
}

class WaitTimeoutException : Exception
{
	this(string msg, string file = __FILE__, size_t line = __LINE__, Throwable nextInChain = null) pure nothrow @nogc @safe
	{
		super(msg, file, line, nextInChain);
	}

	this(string file = __FILE__, size_t line = __LINE__, Throwable nextInChain = null) pure nothrow @nogc @safe
	{
		super("wait timed out without event", file, line, nextInChain);
	}
}

class ChannelNotFoundException : Exception
{
	string channel;

	this(string channel, string file = __FILE__, size_t line = __LINE__, Throwable nextInChain = null) pure @safe
	{
		super("Channel " ~ channel ~ " not found.", file, line, nextInChain);
		this.channel = channel;
	}
}

/// Represents a multiplayer lobby in osu!
/// Automatically does ratelimiting by not sending more than a message every 2 seconds.
///
/// All slot indices are 0 based.
class OsuRoom // must be a class, don't change it
{
	/// Returned by !mp settings
	struct Settings
	{
		/// Represents a player state in the settings result
		struct Player
		{
			/// Player user information, may not be there except for name
			string id, url, name;
			///
			bool ready;
			///
			bool playing;
			///
			bool noMap;
			///
			bool host;
			/// If freemods is enabled this contains user specific mods
			Mod[] mods;
			///
			Team team;

			static Player userTeam(string name, Team team)
			{
				Player ret;
				ret.name = name;
				ret.team = team;
				return ret;
			}
		}

		/// Game name
		string name;
		/// URL to match history
		string history;
		/// Beatmap information
		BeatmapInfo beatmap;
		/// Global active mods or all mods if freemods is off, contains Mod.FreeMod if on
		Mod[] activeMods;
		/// Type of game (coop, tag team, etc.)
		TeamMode teamMode;
		/// Win condition (score, acc, combo, etc.)
		ScoreMode winCondition;
		/// Number of players in this match
		int numPlayers;
		/// All players for every slot (empty slots are Player.init)
		Player[16] players;
	}

	private BanchoBot bot;
	private string _channel, id;
	private bool open;
	private bool fatal;

	/// Automatically managed state of player slots, empty slots are Player.init
	Settings.Player[16] slots;
	/// username as argument
	NothrowEvent!string onUserLeave;
	/// username & team as argument
	NothrowEvent!(string, Team) onUserTeamChange;
	/// username as argument
	NothrowEvent!string onUserHost;
	///
	NothrowEvent!() onHostCleared;
	/// username + slot (0 based) + team as argument
	NothrowEvent!(string, ubyte, Team) onUserJoin;
	/// username + slot (0 based) as argument
	NothrowEvent!(string, ubyte) onUserMove;
	/// emitted when all players are ready
	NothrowEvent!() onPlayersReady;
	/// Match has started
	NothrowEvent!() onMatchStart;
	/// Match has ended (all players finished)
	NothrowEvent!() onMatchEnd;
	/// Host is changing beatmap
	NothrowEvent!() onBeatmapPending;
	/// Host changed map
	NothrowEvent!BeatmapInfo onBeatmapChanged;
	/// A message by anyone has been sent
	NothrowEvent!Message onMessage;
	/// A timer finished
	NothrowEvent!() onCountdownFinished;
	/// A countdown was aborted
	NothrowEvent!() onCountdownAborted;
	/// The match was started but now aborted
	NothrowEvent!() onMatchAborted;
	/// A user finished playing. username + score + passed
	NothrowEvent!(string, long, bool) onPlayerFinished;
	/// The room has been closed
	NothrowEvent!() onClosed;

	bool timerIsStart;
	bool timerRunning;
	bool inProgress;

	private this(BanchoBot bot, string channel, string id)
	{
		assert(channel.startsWith("#mp_"));
		this.bot = bot;
		this._channel = channel;
		this.id = id;
		open = true;
	}

	/// Use slots[i] for a nothrow overload
	ref Settings.Player slot(int index)
	{
		if (index < 0 || index >= 16)
			throw new Exception("slot index out of bounds");
		return slots[index];
	}

	bool hasPlayer(string name) nothrow
	{
		foreach (ref slot; slots)
			if (slot.name == name)
				return true;
		return false;
	}

	ref Settings.Player playerByName(string name)
	{
		foreach (ref slot; slots)
			if (slot.name == name)
				return slot;
		throw new Exception("player " ~ name ~ " not found!");
	}

	ref Settings.Player playerByName(string name, out size_t index)
	{
		foreach (i, ref slot; slots)
			if (slot.name == name)
			{
				index = i;
				return slot;
			}
		throw new Exception("player " ~ name ~ " not found!");
	}

	deprecated ubyte playerSlotByName(string name)
	{
		auto index = playerSlotIndexByName(name);
		if (index == ubyte.max)
			throw new Exception("player " ~ name ~ " not found!");
		else
			return index;
	}

	/// Returns the player slot for the given player username or ubyte.max if not found.
	ubyte playerSlotIndexByName(string name) nothrow
	{
		foreach (i, ref slot; slots)
			if (slot.name == name)
				return cast(ubyte) i;
		return ubyte.max;
	}

	/// Returns the channel name as on IRC
	string channel() const @property nothrow
	{
		return _channel;
	}

	/// Returns the room ID as usable in the mp history URL or IRC joinable via #mp_ID
	string room() const @property nothrow
	{
		return channel["#mp_".length .. $];
	}

	/// Returns the game ID as usable in osu://mp/ID urls
	string mpid() const @property nothrow
	{
		return id;
	}

	/// Returns the BanchoBot instance used with this room.
	BanchoBot bancho() @property nothrow
	{
		return bot;
	}

	/// Closes the room and calls processClosed.
	void close() nothrow
	{
		if (!open)
			return;
		trySendMessage("!mp close");
		processClosed();
	}

	private bool simpleUserCommmand(scope const(char)[] command, scope const(char)[] success)
	{
		return this.commandWithResponse(a => !!a.message.startsWithString(success, "User"), command)
			.message.startsWithString(success);
	}

	/// Adds a player as referee (can manage the room with !mp commands), also useful to allow room control to another bot.
	/// See_Also: addRefs
	/// Throws: NoServerResponseException for no response from bancho.
	/// Returns: true if the player could be made referee or false if not.
	bool addRef(string player)
	{
		// <WebFreak> !mp addref WebFreak
		// <BanchoBot> You have too many referees! Remove some to add new ones.

		// <WebFreak> !mp addref sgerdgergerg
		// <BanchoBot> User not found: sgerdgergerg

		// <WebFreak> !mp addref LazyLea, DasDoge88
		// <BanchoBot> Added LazyLea to the match referees
		// <BanchoBot> Added DasDoge88 to the match referees

		return this.commandWithResponse(a => a.message.canFind("too many referees")
				|| a.message.canFind("not found") || a.message.startsWithString("Added "),
				"!mp addref " ~ player.fixUsername).message.startsWithString("Added ");
	}

	/// Removes a player from referees.
	/// See_Also: removeRefs
	/// Throws: NoServerResponseException for no response from bancho.
	/// Returns: true if the player was removed from referees or false if not.
	bool removeRef(string player)
	{
		// <BanchoBot> Removed DasDoge88 from the match referees

		sendImportantMessage("!mp removeref " ~ player.fixUsername);
		return this.commandWithResponse(a => a.message.canFind("too many referees")
				|| a.message.canFind("not found") || a.message.startsWithString("Removed ")
				|| a.message.endsWith("not a match referee"), "!mp removeref " ~ player.fixUsername).message.startsWithString(
				"Removed ");
	}

	/// Adds players to referees.
	/// See_Also: addRef
	/// Throws: NoServerResponseException for no response from bancho.
	/// Returns: true if any player could be made referee or false if not.
	bool addRefs(string[] players)
	{
		if (players.length)
		{
			return this.commandWithResponse!true(a => a.message.canFind("too many referees")
					|| a.message.canFind("not found") || a.message.startsWithString("Added "),
					"!mp addref " ~ players.map!(a => a.fixUsername).join(", ")).any!(
					a => a.message.startsWithString("Added "));
		}
		return false;
	}

	/// Removes players from referees.
	/// See_Also: removeRef
	/// Throws: NoServerResponseException for no response from bancho.
	/// Returns: true if any player was removed from referees or false if not.
	bool removeRefs(string[] players)
	{
		if (players.length)
		{
			return this.commandWithResponse!true(a => a.message.canFind("too many referees")
					|| a.message.canFind("not found") || a.message.startsWithString("Removed ")
					|| a.message.endsWith("not a match referee"),
					"!mp removeref " ~ players.map!(a => a.fixUsername).join(", ")).any!(
					a => a.message.startsWithString("Removed "));
		}
		return false;
	}

	/// Lists all match referees. Note that potentially this list might not be complete if a message packet gets lost.
	/// Throws: NoServerResponseException for no response from bancho.
	/// Returns: the list of player names who are match referees.
	string[] listRefs()
	{
		bool listing;
		auto ret = this.commandWithResponse!true((a) {
			if (listing)
			{
				return a.message.isValidOsuUsername(19);
			}
			else
				return listing = a.message.startsWithString("Match referees");
		}, despam("!mp listrefs"));
		assert(ret.length, "commandWithResponse always returns at least 1 item");
		string[] messages = new string[ret.length - 1];
		foreach (i, v; ret[1 .. $])
			messages[i] = v.message;
		return messages;
	}

	/// ditto
	string[] tryListRefs() nothrow
	{
		bool listing;
		Message[] ret;
		if (!this.tryCommandWithResponse((a) {
				if (listing)
				{
					return a.message.isValidOsuUsername(19);
				}
				else
					return listing = a.message.startsWithString("Match referees");
			}, ret, despam("!mp listrefs")))
			return null;
		assert(ret.length, "commandWithResponse always returns at least 1 item");
		string[] messages = new string[ret.length - 1];
		foreach (i, v; ret[1 .. $])
			messages[i] = v.message;
		return messages;
	}

	/// Invites a player to the room
	/// Throws: NoServerResponseException for no response from bancho.
	/// Returns: true if the player was found and invited.
	bool invite(string player)
	{
		// <BanchoBot> User not found: Croisssant
		// <BanchoBot> Invited LazyLea to the room
		return simpleUserCommmand("!mp invite " ~ player.fixUsername, "Invited");
	}

	/// Kicks a player from the room
	/// Throws: NoServerResponseException for no response from bancho.
	/// Returns: true if the player was found and kicked.
	bool kick(string player)
	{
		// <BanchoBot> Kicked LazyLea from the match
		// <BanchoBot> User is not in the match!
		return simpleUserCommmand("!mp kick " ~ player.fixUsername, "Kicked");
	}

	/// Moves a player to another slot
	/// Throws: NoServerResponseException for no response from bancho.
	/// Returns: true if the player was found and moved.
	bool move(string player, int slot)
	{
		// <BanchoBot> Moved Kinneshi into slot 14
		// <BanchoBot> User is not in the match room
		return simpleUserCommmand("!mp move " ~ player.fixUsername ~ " " ~ (slot + 1).to!string,
				"Moved");
	}

	/// Gives host to a player
	void host(string player) @property
	{
		sendImportantMessage("!mp host " ~ player.fixUsername);
	}

	/// Makes nobody host (make it system/bot managed)
	/// Throws: NoServerResponseException for no response from bancho.
	void clearhost()
	{
		// <BanchoBot> Cleared match host
		this.commandWithResponse(a => a.message == "Cleared match host", "!mp clearhost");
	}

	/// Property to lock slots (disallow changing slots & joining)
	/// Throws: NoServerResponseException for no response from bancho.
	void locked(bool locked) @property
	{
		string cmd = locked ? "!mp lock" : "!mp unlock";
		string response = locked ? "Locked the match" : "Unlocked the match";
		this.commandWithResponse(a => a.message == response, cmd);
	}

	/// Sets the match password (password will be visible to existing players)
	/// Throws: NoServerResponseException for no response from bancho.
	void password(string pw) @property
	{
		// <BanchoBot> Changed the match password
		// <BanchoBot> Removed the match password
		this.commandWithResponse(a => a.message.endsWith("match password"), "!mp password " ~ pw);
	}

	/// Changes a user's team
	/// Throws: NoServerResponseException for no response from bancho.
	/// Returns: true if the player was found and moved to the team.
	bool setTeam(string user, Team team)
	{
		// <BanchoBot> Moved WebFreak to team Red
		// <BanchoBot> Moved WebFreak to team Blue
		return simpleUserCommmand("!mp team " ~ user.fixUsername ~ " " ~ team.to!string, "Moved");
	}

	/// Changes the slot limit of this lobby
	/// Throws: NoServerResponseException for no response from bancho.
	void size(ubyte slots) @property
	{
		// <WebFreak> !mp size 0
		// <BanchoBot> Changed match to size 1
		// <WebFreak> !mp size 20
		// <BanchoBot> Changed match to size 16
		// <WebFreak> !mp size e
		// <BanchoBot> Invalid or no size provided
		this.commandWithResponse(a => !!a.message.startsWithString("Changed match to size",
				"Invalid or no size provided"), "!mp size " ~ slots.to!string);
	}

	/// Sets up teammode, scoremode & lobby size
	/// Throws: NoServerResponseException for no response from bancho.
	void set(TeamMode teammode, ScoreMode scoremode, ubyte size)
	{
		// <BanchoBot> Changed match settings to 16 slots, TagTeamVs, Score
		// <BanchoBot> Invalid or no settings provided
		this.commandWithResponse(a => !!a.message.startsWithString("Changed match settings to",
				"Invalid or no settings"), "!mp set " ~ (cast(int) teammode)
				.to!string ~ " " ~ (cast(int) scoremode).to!string ~ " " ~ size.to!string);
	}

	/// Changes the mods in this lobby (pass FreeMod first if you want FreeMod)
	/// Throws: NoServerResponseException for no response from bancho.
	void mods(Mod[] mods) @property
	{
		this.commandWithResponse(a => !!a.message.startsWithString("Enabled",
				"Disabled"), "!mp mods " ~ mods.map!(a => a.shortForm).join(" "));
	}

	/// Changes the mods in this lobby with their enum value. Different mods are supported and FreeMod is not supported, but more combinations and other mods can be picked with this.
	/// Throws: NoServerResponseException for no response from bancho.
	void mods(ModNumber mods) @property
	{
		this.commandWithResponse(a => !!a.message.startsWithString("Enabled",
				"Disabled"), "!mp mods " ~ (cast(uint) mods).to!string);
	}

	/// Changes the map to a beatmap ID (b/ url)
	/// Throws: NoServerResponseException for no response from bancho.
	/// Returns: true if the map was found and changed.
	bool map(string id) @property
	{
		// <BanchoBot> Invalid map ID provided
		// <BanchoBot> Changed beatmap to https://osu.ppy.sh/b/100 Phantasma - Fall Memories
		return this.commandWithResponse(a => !!a.message.startsWithString("Invalid map ID",
				"Changed beatmap to"), "!mp map " ~ id).message.startsWithString("Changed beatmap to");
	}

	/// Changes the map to a beatmap ID (b/ url) with gamemode
	/// Throws: NoServerResponseException for no response from bancho.
	/// Returns: true if the map was found and changed.
	bool mapAndMode(string id, GameMode mode) @property
	{
		return this.commandWithResponse(a => !!a.message.startsWithString("Invalid map ID",
				"Changed beatmap to"), "!mp map " ~ id ~ " " ~ (cast(int) mode).to!string).message.startsWithString(
				"Changed beatmap to");
	}

	/// Sets a timer using !mp timer
	/// Throws: NoServerResponseException for no response from bancho.
	void setTimer(Duration d)
	{
		setTimerImpl!true(d);
	}
	/// ditto
	bool trySetTimer(Duration d) nothrow
	{
		return setTimerImpl!false(d);
	}

	private auto setTimerImpl(bool force)(Duration d)
	{
		if (d.total!"seconds" > 0)
		{
			auto oldTimerRunning = timerRunning;
			auto oldTimerIsStart = timerIsStart;
			scope (failure)
			{
				timerRunning = oldTimerRunning;
				timerIsStart = oldTimerIsStart;
			}

			timerRunning = true;
			timerIsStart = false;

			static if (force)
			{
				this.commandWithResponse(a => !!a.message.startsWithString("Countdown ends in"),
						"!mp timer " ~ d.total!"seconds".to!string);
			}
			else
			{
				Message dummy;
				return this.tryCommandWithResponse(a => !!a.message.startsWithString("Countdown ends in"),
						dummy,
						"!mp timer " ~ d.total!"seconds".to!string);
			}
		}
		else
		{
			static if (!force)
				return true;
		}
	}

	/// Waits for a player to join the room & return the username
	/// Throws: WaitTimeoutException if timeout triggers
	string waitForJoin(Duration timeout)
	{
		auto l = bot.waitForMessage(a => a.target == channel && a.sender == banchoBotNick
				&& a.message.representation.canFind(" joined in slot ".representation), timeout).message;
		auto i = l.indexOf(" joined in slot ");
		return l[0 .. i];
	}

	/// Waits for a player to join the room & return the username or `null` if
	/// nobody joined within the timeout.
	string tryWaitForJoin(Duration timeout) nothrow
	{
		Message msg;
		if (!bot.tryWaitForMessage(a => a.target == channel && a.sender == banchoBotNick
				&& a.message.representation.canFind(" joined in slot ".representation), timeout, msg))
			return null;
		auto txt = msg.message;
		auto i = txt.indexOf(" joined in slot ");
		return txt[0 .. i];
	}

	/// Waits for an existing timer/countdown to finish (wont start one)
	/// Throws: WaitTimeoutException if timeout triggers
	void waitForTimer(Duration timeout)
	{
		bot.waitForMessage(a => a.target == channel && a.sender == banchoBotNick
				&& a.message == "Countdown finished", timeout);
	}

	/// ditto
	bool tryWaitForTimer(Duration timeout) nothrow
	{
		Message dummy;
		return bot.tryWaitForMessage(a => a.target == channel && a.sender == banchoBotNick
				&& a.message == "Countdown finished", timeout, dummy);
	}

	/// Aborts any running countdown
	void abortTimer()
	{
		return abortTimerImpl!true;
	}

	/// ditto
	bool tryAbortTimer() nothrow
	{
		return abortTimerImpl!false;
	}

	private auto abortTimerImpl(bool important)()
	{
		// doesn't send any message without timer running
		scope (success)
			timerRunning = false;
		return sendMessageForce!important("!mp aborttimer");
	}

	/// Aborts a running match
	/// Throws: NoServerResponseException for no response from bancho
	/// Returns: true if the match was running and got aborted or after a timeout of 3 seconds.
	bool abortMatch()
	{
		auto msg = this.commandWithResponse(a => a.message == "The match is not in progress"
				|| a.message == "Aborted the match", "!mp abort");

		return msg.message != "The match is not in progress";
	}

	/// Starts a match after a specified amount of seconds. If after is <= 0 the game will be started immediately.
	/// The timeout can be canceled using abortTimer.
	/// Throws: NoServerResponseException for no response from bancho
	/// Returns: false if the match was already started, true otherwise.
	bool start(Duration after = Duration.zero)
	{
		if (after <= Duration.zero)
		{
			// <BanchoBot> The match has already been started
			// <BanchoBot> The match has started!
			return !this.commandWithResponse(a => a.message.startsWithString("The match"),
					"!mp start").message.endsWith("already been started");
		}
		else
		{
			// <BanchoBot> Match starts in
			// <BanchoBot> The match has already been started

			auto oldTimerRunning = timerRunning;
			auto oldTimerIsStart = timerIsStart;
			scope (failure)
			{
				timerRunning = oldTimerRunning;
				timerIsStart = oldTimerIsStart;
			}

			timerRunning = true;
			timerIsStart = true;
			return !this.commandWithResponse(a => !!a.message.startsWithString("The match",
					"Match starts in"), "!mp start " ~ after.total!"seconds"
					.to!string).message.endsWith("already been started");
		}
	}

	/// Manually wait until you can send a message again.
	/// Params:
	///    put = if false, the time won't get written to the ratelimit and it will just be waited for it.
	void ratelimit(bool put)
	{
		bot.ratelimit(channel, put);
	}

	/// ditto
	bool ratelimitNothrow(bool put) nothrow
	{
		return bot.ratelimitNothrow(channel, put);
	}

	auto sendMessageForce(bool important)(scope const(char)[] message)
	{
		static if (important)
			return sendImportantMessage(message);
		else
			return trySendMessage(message);
	}

	deprecated alias sendMessage = sendImportantMessage;

	/// Sends a message with a 2 second ratelimit
	/// Throws: Exception when attempting to send a message in a closed room or
	/// if there is a socket exception or
	/// InterruptException if task is interrupted while sending
	/// (none if trySendMessage /  is used)
	/// Params:
	///   message = raw message to send
	void sendImportantMessage(scope const(char)[] message)
	{
		if (open)
			ratelimitNothrow(true);
		if (!open)
			throw new Exception("Attempted to send message in closed room");
		bot.sendMessage(channel, message, false);
	}

	/// ditto
	bool trySendMessage(scope const(char)[] message) nothrow
	{
		if (open)
			ratelimitNothrow(true);
		if (open)
			return bot.trySendMessage(channel, message, false);
		return false;
	}

	/// Returns the current mp settings
	Settings settings() @property
	{
		int step = 0;
	Retry:
		bot.fetchOldMessageLog(a => a.target == channel && a.sender == banchoBotNick
				&& a.message.isSettingsMessage, false);
		sendImportantMessage("!mp settings");
		auto msgs = bot.waitForMessageBunch(a => a.target == channel
				&& a.sender == banchoBotNick && a.message.isSettingsMessage, 10.seconds,
				10.seconds, 400.msecs);
		if (!msgs.length)
			return Settings.init;
		Settings settings;
		settings.numPlayers = -1;
		int foundPlayers;
		SettingsLoop: foreach (msg; msgs)
		{
			if (msg.message.startsWithString("Room name: "))
			{
				// Room name: bob, History: https://osu.ppy.sh/mp/40123558
				msg.message = msg.message["Room name: ".length .. $];
				auto end = msg.message.indexOf(", History: ");
				if (end != -1)
				{
					settings.name = msg.message[0 .. end];
					settings.history = msg.message[end + ", History: ".length .. $];
				}
			}
			else if (msg.message.startsWithString("Beatmap: "))
			{
				// Beatmap: https://osu.ppy.sh/b/972293 Ayane - FaV -F*** and Vanguard- [Normal]
				msg.message = msg.message["Beatmap: ".length .. $];
				auto space = msg.message.indexOf(" ");
				if (space != -1)
				{
					settings.beatmap.url = msg.message[0 .. space];
					if (settings.beatmap.url.startsWith("https://osu.ppy.sh/b/"))
						settings.beatmap.id = settings.beatmap.url["https://osu.ppy.sh/b/".length .. $];
					else
						settings.beatmap.id = "";
					settings.beatmap.name = msg.message[space + 1 .. $];
				}
			}
			else if (msg.message.startsWithString("Team mode: "))
			{
				// Team mode: TeamVs, Win condition: ScoreV2
				msg.message = msg.message["Team mode: ".length .. $];
				auto comma = msg.message.indexOf(", Win condition: ");
				if (comma != -1)
				{
					settings.teamMode = msg.message[0 .. comma].to!TeamMode;
					settings.winCondition = msg.message[comma + ", Win condition: ".length .. $]
						.to!ScoreMode;
				}
			}
			else if (msg.message.startsWithString("Active mods: "))
			{
				// Active mods: Hidden, DoubleTime
				settings.activeMods = msg.message["Active mods: ".length .. $].splitter(", ")
					.map!(a => cast(Mod) a).array;
			}
			else if (msg.message.startsWithString("Players: "))
			{
				// Players: 1
				settings.numPlayers = msg.message["Players: ".length .. $].to!int;
			}
			else if (msg.message.startsWithString("Slot "))
			{
				foundPlayers++;
				// Slot 1  Not Ready https://osu.ppy.sh/u/1756786 WebFreak        [Host / Team Blue / Hidden, HardRock]
				// Slot 1  Ready     https://osu.ppy.sh/u/1756786 WebFreak        [Host / Team Blue / NoFail, Hidden, HardRock]
				//"Slot 1  Not Ready https://osu.ppy.sh/u/1756786 WebFreak        "
				if (msg.message.length < SettingsLineMinLength)
					continue;
				auto num = msg.message[5 .. 7].strip.to!int;
				msg.message = msg.message.stripLeft;
				if (num >= 1 && num <= 16)
				{
					auto index = num - 1;
					settings.players[index].ready = msg.message[8 .. 17] == "Ready    ";
					settings.players[index].noMap = msg.message[8 .. 17] == "No Map   ";
					settings.players[index].url = msg.message[18 .. $];
					auto space = settings.players[index].url.indexOf(' ');
					if (space == -1)
						continue;
					auto rest = settings.players[index].url[space + 1 .. $];
					settings.players[index].url.length = space;
					settings.players[index].id = settings.players[index].url[settings.players[index].url.lastIndexOf(
								'/') + 1 .. $];
					auto bracket = rest.indexOf("[", 16);
					if (bracket == -1)
						settings.players[index].name = rest.stripRight;
					else
					{
						settings.players[index].name = rest[0 .. bracket].stripRight;
						auto extra = rest[bracket + 1 .. $];
						if (extra.endsWith("]"))
							extra.length--;
						foreach (part; extra.splitter(" / "))
						{
							if (part == "Host")
								settings.players[index].host = true;
							else if (part.startsWith("Team "))
								settings.players[index].team = part["Team ".length .. $].strip.to!Team;
							else if (part == "None")
								settings.players[index].mods = null;
							else
								settings.players[index].mods = part.splitter(", ").map!(a => cast(Mod) a).array;
						}
					}
				}
			}
		}
		if ((foundPlayers < settings.numPlayers || settings.numPlayers == -1) && ++step < 5)
		{
			msgs = bot.waitForMessageBunch(a => a.target == channel && a.sender == banchoBotNick
					&& a.message.isSettingsMessage, 3.seconds, 3.seconds, 600.msecs);
			if (msgs.length)
				goto SettingsLoop;
		}
		if (foundPlayers && settings.numPlayers <= 0 && step < 5)
			goto Retry;
		if (settings.numPlayers == -1)
			return settings;
		slots = settings.players;
		return settings;
	}

	/// Processes a user leave event & updates the state
	void processLeave(string user)
	{
		try
		{
			playerByName(user) = Settings.Player.init;
			runTask(() @safe nothrow { onUserLeave.emit(user); });
		}
		catch (Exception)
		{
		}
	}

	/// Processes a user team switch event & updates the state
	void processTeam(string user, Team team)
	{
		try
		{
			playerByName(user).team = team;
			runTask(() @safe nothrow { onUserTeamChange.emit(user, team); });
		}
		catch (Exception)
		{
		}
	}

	/// Processes a user host event & updates the state
	void processHost(string user)
	{
		foreach (ref slot; slots)
			slot.host = false;
		try
		{
			playerByName(user).host = true;
			runTask(() @safe nothrow { onUserHost.emit(user); });
		}
		catch (Exception)
		{
		}
	}

	/// Processes a host clear event & updates the state
	void processHostClear()
	{
		foreach (ref slot; slots)
			slot.host = false;
		runTask(() @safe nothrow { onHostCleared.emit(); });
	}

	/// Processes a user join event & updates the state
	void processJoin(string user, ubyte slot, Team team)
	{
		this.slot(slot) = Settings.Player.userTeam(user, team);
		runTask(() @safe nothrow { onUserJoin.emit(user, slot, team); });
	}

	/// Processes a user move event & updates the state
	void processMove(string user, ubyte slot)
	{
		if (this.slot(slot) != Settings.Player.init)
			throw new Exception("slot was occupied");
		size_t old;
		this.slot(slot) = playerByName(user, old);
		this.slot(cast(int) old) = Settings.Player.init;
		runTask(() @safe nothrow { onUserMove.emit(user, slot); });
	}

	/// Processes a room size change event & updates the state
	void processSize(ubyte numSlots)
	{
		foreach (i; numSlots + 1 .. 16)
			if (slots[i] != Settings.Player.init)
			{
				runTask((string user) { onUserLeave.emit(user); }, slots[i].name);
				slots[i] = Settings.Player.init;
			}
	}

	/// Processes a match end event & updates the state
	void processMatchFinish()
	{
		foreach (ref slot; slots)
			if (slot != Settings.Player.init)
				slot.playing = false;
		inProgress = false;
		runTask(() @safe nothrow { onMatchEnd.emit(); });
	}

	/// Processes a user finish playing event & updates the state
	void processFinishPlaying(string player, long score, bool pass)
	{
		playerByName(player).playing = false;
		inProgress = false;
		runTask(() @safe nothrow { onPlayerFinished.emit(player, score, pass); });
	}

	/// Processes a room closed event
	void processClosed() nothrow
	{
		open = false;
		bot.unmanageRoom(this);
		runTask(() @safe nothrow { onClosed.emit(); });
	}

	/// Returns: false if a close has been processed or close() has been called.
	bool isOpen() const @property nothrow
	{
		return open;
	}

	/// Repeats the callback parameter until it doesn't throw
	/// NoServerResponseException or the room is closed.
	void repeatUntilResponded(Callback)(Callback cb, Duration idleTimeout = 10.seconds, size_t retries = -1)
	{
		while (open && retries)
		{
			try
			{
				cb();
				return;
			}
			catch (NoServerResponseException)
			{
			}
			sleep(idleTimeout);
			retries--;
		}
	}

	/// Same as $(LREF repeatUntilResponded) but exits with an AssertError if
	/// the exception is anything else than NoServerResponseException or
	/// InterruptException.
	void repeatUntilRespondedOrFatalExit(Callback)(Callback cb, Duration idleTimeout = 10.seconds, size_t retries = -1,
		string file = __FILE__, size_t line = __LINE__) nothrow
	{
		try
		{
			repeatUntilResponded(cb, idleTimeout, retries);
		}
		catch (InterruptException)
		{
		}
		catch (Exception e)
		{
			import core.exception : AssertError;

			throw new AssertError("callback in repeatUntilRespondedOrFatalExit has thrown something unexpected", file, line, e);
		}
	}
}

bool isSettingsMessage(string msg) nothrow
{
	import std.ascii : isDigit;

	return msg.startsWithString("Room name: ", "Beatmap: ", "Team mode: ", "Active mods: ", "Players: ", "Slot ")
				&& msg.length >= SettingsLineMinLength
				&& ("Slot ".length < msg.length && msg["Slot ".length].isDigit);
}

/// Returns the value given in first parameter or executes the callback if
/// evaluating it threw a WaitTimeoutException. If any other exception is
/// thrown, an AssertError will be triggered.
T orTimeout(T, Cb)(lazy T value, Cb cb, string file = __FILE__, size_t line = __LINE__)
{
	try
	{
		return value;
	}
	catch (WaitTimeoutException)
	{
	}
	catch (Exception e)
	{
		import core.exception : AssertError;

		throw new AssertError("callback in repeatUntilRespondedOrFatalExit has thrown something unexpected", file, line, e);
	}
}

unittest
{
	BanchoBot banchoConnection = new BanchoBot("WebFreak", "invalid");
	assert(!banchoConnection.connect());
}

///
unittest
{
	import bancho.irc;

	void main()
	{
		BanchoBot banchoConnection = new BanchoBot("WebFreak", "");
		bool running = true;
		auto botTask = runTask(() @safe nothrow {
			while (running)
			{
				banchoConnection.connect();
				logDiagnostic("Got disconnected from bancho...");

				if (waitForInterrupt(2.seconds))
					break;
			}
		});
		sleep(3.seconds);
		auto users = ["WebFreak", "Node"];
		OsuRoom room = banchoConnection.createRoom("bob");
		sleep(7.seconds);
		room.password = "123456";
		runTask(() @safe nothrow {
			// repeats code until bancho responded to everything (didn't throw)
			// aborts when room is closed
			room.repeatUntilRespondedOrFatalExit({
				room.size = 8;
				room.mods = [Mod.Hidden, Mod.DoubleTime];
				room.map = "1158325";
			});
		});
		runTask(() @safe nothrow {
			foreach (user; users)
				room.repeatUntilRespondedOrFatalExit({
					room.invite(user);
				});
		});
		runTask(() @safe nothrow {
			int joined;
			while (true)
			{
				string user = room.tryWaitForJoin(30.seconds);
				if (user.length)
				{
					joined++;
					room.trySendMessage("yay welcome " ~ user ~ "!");
				}
				else
				{
					if (joined == 0)
					{
						// forever alone
						room.close();
						return;
					}
					else
					{
						break;
					}
				}
			}

			room.trySendMessage("Referees: " ~ room.tryListRefs.join(", "));
			room.trySendMessage(
				"This is an automated test, this room will close in 10 seconds on timer (three times)");
			room.trySetTimer(10.seconds);
			room.trySetTimer(10.seconds);
			room.trySetTimer(10.seconds);
			room.waitForTimer(15.seconds).orTimeout({
				room.trySendMessage("Timer didn't trigger :(");
				room.trySendMessage("closing the room in 5s");
				waitForInterrupt(5.seconds);
			});
			room.close();
		}).join();
		running = false;
		banchoConnection.disconnect();
		botTask.join();
	}
}

private bool startsWithString(scope const(char)[] doesThis, scope const(char)[][] startWith...) @safe nothrow
{
	foreach (that; startWith)
		if (doesThis.length >= that.length && doesThis[0 .. that.length] == that)
			return true;
	return false;
}

nothrow unittest
{
	assert(!startsWithString("hello", "hellof"));
	assert(startsWithString("hello", "a", "b", "he"));
}
